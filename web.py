#20.59.57.118
import asyncio, aiohttp.web, math, ssl, aiohttp_cors, builtins

async def episode(request):
    async with aiohttp.ClientSession() as client:
        async with client.get('https://huggingface.co/api/models/chaowenguo/video') as models: return aiohttp.web.Response(text=builtins.next(builtins.iter((await builtins.anext(_  for _ in builtins.reversed((await models.json()).get('siblings')) if await request.text() in builtins.next(builtins.iter(_.values())))).values())).split('/')[-1].split('.')[0])

async def list(request):
    async with aiohttp.ClientSession() as client:
        async with client.get('https://huggingface.co/api/models/chaowenguo/video') as models: return aiohttp.web.json_response([*{value.split('/')[1] for _ in (await models.json()).get('siblings') if await request.text() in (value := builtins.next(builtins.iter(_.values())))}])

async def mp4(request):
    json = await request.json()
    async with aiohttp.ClientSession() as client:
        async with client.get(f'https://huggingface.co/chaowenguo/video/resolve/main/{json.get("drama")}/{json.get("episode")}.mp4', allow_redirects=False) as  _: return aiohttp.web.Response(text=_.headers.get('Location'))

async def vtt(request):
    json = await request.json()
    async with aiohttp.ClientSession() as client:
        async with client.get(f'https://huggingface.co/chaowenguo/video/raw/main/{json.get("drama")}/{json.get("episode")}.vtt') as  _: return aiohttp.web.Response(text=await _.text())

async def main():
    app = aiohttp.web.Application()
    cors = aiohttp_cors.setup(app, defaults={'*': aiohttp_cors.ResourceOptions(allow_credentials=True, expose_headers='*', allow_headers='*')})
    cors.add(app.router.add_post('/episode', episode))
    cors.add(app.router.add_post('/list', list))
    cors.add(app.router.add_post('/mp4', mp4))
    cors.add(app.router.add_post('/', vtt))
    runner = aiohttp.web.AppRunner(app)
    await runner.setup()
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain('/etc/ssl/certs/cert.pem', '/etc/ssl/private/key.pem')
    site = aiohttp.web.TCPSite(runner, port=443, ssl_context=ssl_context)
    await site.start()
    await asyncio.sleep(math.inf)

asyncio.run(main())